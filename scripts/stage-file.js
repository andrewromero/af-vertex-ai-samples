const { GoogleAuth } = require('google-auth-library');
const { Storage } = require('@google-cloud/storage');


const svcAcctKeyPath = '../keys/af-samples.json'
const imgLocalPath = 'img/adonis/1.jpg'
const gcsBucketName = 'andrew-altostrat'
const gcsKey = 'adonis1.jpg'

const clientOptions = {
    auth: new GoogleAuth({
        keyFile: svcAcctKeyPath,
        scopes: 'https://www.googleapis.com/auth/cloud-platform',
    })
};

const storage = new Storage(clientOptions);

const now = new Date()
gcsDir = 'image-stage/' + now.toISOString().replace(/\D/g,'').slice(0, 10) + '/'

async function uploadFile() {
    await storage.bucket(gcsBucketName).upload(imgLocalPath, {
        destination: gcsDir + gcsKey,
    });
    console.log(`${imgLocalPath} uploaded to ${gcsBucketName}`);
}

uploadFile().catch(console.error);
