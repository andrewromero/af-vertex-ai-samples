const { Storage } = require('@google-cloud/storage');
const aiplatform = require('@google-cloud/aiplatform');
const { JobServiceClient } = require('@google-cloud/aiplatform').v1
const { params } = aiplatform.protos.google.cloud.aiplatform.v1.schema.predict;

const location = 'us-central1';
const projId = 'andrew-altostrat';
const gcsBucketName = 'andrew-altostrat-vertex';
const modelId = '9019782065809260544';
const batchSize = 5

const parent = `projects/${projId}/locations/${location}`;
const modelName = `projects/${projId}/locations/${location}/models/${modelId}`;
const gcsOutput = `gs://${gcsBucketName}/predictions`


const clientOptions = {
    apiEndpoint: 'us-central1-aiplatform.googleapis.com',
};

const vertex = new JobServiceClient(clientOptions);
const storage = new Storage();
const bucket = storage.bucket(gcsBucketName)


async function main() {
    const now = new Date().toISOString().replace(/\D/g, '').slice(0, 14)

    let gcsKeys = await listFiles();
    gcsKeys = gcsKeys.map(key => JSON.stringify({ content: key, mimeType: 'image/jpeg' })).join('\n')
    batchFile = `batch-lines/batch-input-${now}.json`;
    await bucket.file(batchFile).save(gcsKeys);

    // update to object detection params
    const modelParameters = new params.ImageClassificationPredictionParams({
        confidenceThreshold: 0.5,
    });

    const inputConfig = {
        instancesFormat: 'jsonl',
        gcsSource: { uris: [`gs://${gcsBucketName}/${batchFile}`] },
    };
    const outputConfig = {
        predictionsFormat: 'jsonl',
        gcsDestination: { outputUriPrefix: gcsOutput },
    };

    const batchPredictionJob = {
        displayName: 'predictions-' + now,
        model: modelName,
        modelParameters,
        inputConfig,
        outputConfig
    };
    const request = { parent, batchPredictionJob };
    const [response] = await vertex.createBatchPredictionJob(request);
}

async function listFiles() {
    return await bucket.getFiles({ prefix: 'image-stage/', maxResults: batchSize }).then(
        el => el[0].map(item => `gs://${gcsBucketName}/` + item.name)
    )
}

main().catch(console.log);
